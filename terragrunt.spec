%define debug_package %{nil}

Name:    terragrunt
Version: 0.69.13
Release: 1%{?dist}
Summary: Terragrunt is a thin wrapper for Terraform
Group:   Applications/System
License: MIT
URL:     https://terragrunt.gruntwork.io/
Source0: https://github.com/gruntwork-io/%{name}/archive/v%{version}.tar.gz
Source1: https://github.com/gruntwork-io/%{name}/releases/download/v%{version}/%{name}_linux_amd64

%description
Terragrunt is a thin wrapper for Terraform that provides extra tools for keeping
your Terraform configurations DRY, working with multiple Terraform modules, and
managing remote state.

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE.txt
%doc README.md
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue Apr 30 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Apr 11 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.55.11

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.54.12

* Tue Nov 14 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.53.3

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.50.9

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.48.4

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.47.0

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.45.13

* Mon May 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.45.10

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.45.4

* Sat Feb 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.44.0

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.43.2

* Wed Dec 14 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.42.5

* Fri Nov 25 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.41.0

* Tue Nov 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.40.2

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.39.2

* Sat Sep 24 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.38.12

* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
